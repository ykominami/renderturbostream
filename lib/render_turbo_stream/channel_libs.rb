module RenderTurboStream
  class ChannelLibs

    def initialize
      @test_responses = []
    end

    def render_to_channel(channel, target, action, partial: nil, template: nil, locals: nil)

      _locals = (locals ? locals : {})

      # fetch target-id

      target_id = RenderTurboStream::Libs.target_to_target_id(target)
      if !target.present? || Rails.env.test?
        if partial.present?
          rendered_html = RenderController.render(partial: partial, locals: _locals, layout: false)
        elsif template.present?
          rendered_html = RenderController.render(template: template, locals: _locals, layout: false)
        end
        if !target.present?
          r = RenderTurboStream::Libs.fetch_arguments_from_rendered_string(rendered_html)
          target_id = r[:target_id]
          target = r[:target]
          Rails.logger.debug("  • Target #{r[:target]} found in #{partial.to_s + template.to_s}")
        end
      end

      unless target.present? && target_id.present?
        raise 'No target specified by arguments and no target found inside the rendered partial'
      end

      # add headers for test

      if Rails.env.test?
        test_response = {
          target: target,
          action: action,
          type: 'channel-partial',
          locals: locals,
          channel: channel,
          html_response: rendered_html.to_s
        }
        test_response[:partial] = partial if partial
        test_response[:template] = template if template
        @test_responses.push(test_response)
      end

      # send

      if partial.present?
        Turbo::StreamsChannel.send(
          "broadcast_#{action}_to",
          channel.to_s,
          target: target_id,
          partial: partial,
          locals: _locals&.symbolize_keys,
          layout: false
        )
      elsif template.present?
        Turbo::StreamsChannel.send(
          "broadcast_#{action}_to",
          channel.to_s,
          target: target_id,
          template: template,
          locals: _locals&.symbolize_keys,
          layout: false
        )
      end
    end

    def action_to_channel(channel, command, arguments)

      if Rails.env.test?

        test_response = {
          action: command,
          type: 'channel-command',
          channel: channel,
          array: [command] + arguments,
        }
        if RenderTurboStream::Test::Request::Libs.first_arg_is_html_id(command)
          target_id = (arguments.first[0..0] == '#' ? arguments.first[1..-1] : arguments.first)
          test_response[:target] = RenderTurboStream::Libs.target_id_to_target(target_id)
        end
        @test_responses.push(test_response)
      end

      content = RenderController.render(template: 'render_turbo_stream_command', layout: false, locals: { command: command, arguments: arguments })

      Turbo::StreamsChannel.broadcast_stream_to(
        channel,
        content: content
      )
    end

    def send_actions_to_channel(channel, actions)
      actions.each do |a|
        if a.is_a?(Array)
          action_to_channel(channel, a.first, a[1..-1])
        else
          render_to_channel(channel, a[:target], a[:action], partial: a[:partial], template: a[:template], locals: a[:locals])
        end
      end
    end

    def self.fetch_partials_variables(relative_path) end

    def test_responses
      @test_responses || []
    end

  end
end