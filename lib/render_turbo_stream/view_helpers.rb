module RenderTurboStream
  module ViewHelpers

    def turbo_target_tag(id = nil, &block)
      content_tag :'turbo-target', id: target_id(id) do
        if block_given?
          capture(&block)
        end
      end
    end

    def target_id(id = nil)
      libs = RenderTurboStream::Libs
      virt_path = self.instance_variable_get('@current_template').instance_variable_get('@virtual_path')
      obj = (id ? id : eval("@#{controller_name.singularize}"))
      libs.target_id(virt_path, obj)
    end

    def last_saved_object
      GlobalID::Locator.locate(session[:last_saved_object])
    end

  end
end