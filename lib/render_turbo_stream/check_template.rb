module RenderTurboStream
  class CheckTemplate

    def initialize(partial: nil, template: nil, available_instance_variables: nil, action: nil)
      if !partial && !template
        raise 'missing attribute partial xor template'
      end
      unless action
        raise 'missing required attribute: action'
      end
      @action = action
      @partial_path = partial
      @template_path = template
      @available_instance_variables = nil #available_instance_variables #=> because if an instance variable was not set on production and the first call, and it was set on a later call, the gem would not know that and not set it.
      prt = (partial ? partial : template).split('/')
      if prt.length < 2
        raise 'Partial or template path must always be specified with the controller path, for example «articles/partial_name».'
      end
      @controller_path = prt[0..-2].join('/')
      @key = relative_path(partial: partial, template: template)
      if production?
        $render_turbo_stream_check_templates ||= {}
        if $render_turbo_stream_check_templates[@key]
          @result = $render_turbo_stream_check_templates[@key]
        else
          @result = { instance_variables: [] }
          fetch_nested_partials(1, partial: @partial_path, template: @template_path)
          $render_turbo_stream_check_templates[@key] ||= @result
        end
      else
        @result = { instance_variables: [] }
        fetch_nested_partials(1, partial: @partial_path, template: @template_path)
      end
    end

    def templates_instance_variables
      @result[:instance_variables]
    end

    private

    def production?
      # in production this config should be set to true
      !Rails.application.config.assets.compile
    end

    def fetch_nested_partials(stack_level, partial: nil, template: nil)

      @nested_partials_done ||= []
      if @nested_partials_done.include?(partial || template)
        return # prevent endless loops
      end
      @nested_partials_done.push(partial || template)
      absolute_path = absolute_path(partial: partial, template: template)
      code = File.read(absolute_path)

      _code = code.dup
      _code.scan(/@[a-z_]+/).each do |var|
        if !@available_instance_variables || (@available_instance_variables && @available_instance_variables.include?(var.to_sym))
          unless @result[:instance_variables].include?(var.to_sym)
            @result[:instance_variables].push(var.to_sym)
          end
        end
      end

      loop do
        _matched_partial = _code.match((/\brender[ ](partial:|())(|[ ])("|')[a-z_\/]+("|')/))
        _code = _code[(_code.index(_matched_partial.to_s) + 5)..-1]
        if _matched_partial

          found_partial = _matched_partial.to_s.split(/("|')/)[-2]
          absolute_path = absolute_path(partial: found_partial)

          @nested_partials ||= []
          @nested_partials.push(absolute_path)
          fetch_nested_partials(stack_level += 1, partial: found_partial)

        else
          break
        end
      end
      renders = code.match((/\brender[ ](partial:|())(|[ ])("|')[a-z_\/]+("|')/)) # scan function didnt  work here, so built a loop
    end

    def absolute_path(partial: nil, template: nil)
      divided = relative_path(partial: partial, template: template).split('/')
      view_folder = divided[0..-2].join('/')
      folder = Rails.root.join('app', 'views', view_folder)
      items = Dir.glob "#{folder.to_s}/*"
      item = items.select { |i| i.split('/').last.match(/^#{divided.last}/) }.first
      unless item.present?
        raise "#{partial ? 'Partial' : 'Template'} not found => #{relative_path(partial: partial, template: template)}"
      end
      item
    end

    def relative_path(partial: nil, template: nil)
      divided = (partial ? partial : template).split('/')
      if divided.length <= 1
        _divided = [@controller_path] + divided
      else
        _divided = divided
      end
      view_folder = _divided[0..-2].join('/')
      file = "#{partial ? '_' : ''}#{divided.last}"
      [view_folder, file].join('/')
    end

  end
end