module RenderTurboStream
  module Test
    module Request
      module ChannelHelpers

        # Assert a action by turbo streams channel to the current_user

        def assert_channel_to_me(user, target_id, action: nil, count: 1, &block)

          channel = "authenticated_user_#{user.id}"

          libs = RenderTurboStream::Test::Request::Libs.new(response)

          r = libs.select_responses(channel, RenderTurboStream::Libs.target_id_to_target(target_id), action, &block)

          assert(
            r[:responses].length == count,
            libs.class.assert_error_message(count, r[:responses].length, r[:log])
          )
        end

        def assert_action_to_me(user, action, count: 1, &block)

          channel = "authenticated_user_#{user.id}"

          libs = RenderTurboStream::Test::Request::Libs.new(response)

          r = libs.select_actions(channel, action, &block)

          assert(
            r[:responses].length == count,
            libs.class.assert_error_message(count, r[:responses].length, r[:log])
          )
        end

        # Assert a action by turbo streams channel to a group of authenticated users

        def assert_channel_to_authenticated_group(group, target_id, count: 1, &block)

          channel = "authenticated_group_#{group}"

          libs = RenderTurboStream::Test::Request::Libs.new(response)

          r = libs.select_responses(channel, target_id, action, &block)

          assert(
            r[:responses].length == count,
            libs.class.assert_error_message(count, r[:responses].length, r[:log])
          )
        end

        def assert_action_to_authenticated_group(group, action, count: 1, &block)

          channel = "authenticated_group_#{group}"

          libs = RenderTurboStream::Test::Request::Libs.new(response)

          r = libs.select_actions(channel, action, &block)

          assert(
            r[:responses].length == count,
            libs.class.assert_error_message(count, r[:responses].length, r[:log])
          )
        end

        # Assert a action by turbo streams channel to all visitors of the page

        def assert_channel_to_all(target_id, action: nil, count: 1, &block)

          libs = RenderTurboStream::Test::Request::Libs.new(response)

          r = libs.select_responses('all', RenderTurboStream::Libs.target_id_to_target(target_id), action, &block)

          assert(
            r[:responses].length == count,
            libs.class.assert_error_message(count, r[:responses].length, r[:log])
          )
        end

        def assert_action_to_all(action, count: 1, &block)

          libs = RenderTurboStream::Test::Request::Libs.new(response)

          r = libs.select_actions('all', action, &block)

          assert(
            r[:responses].length == count,
            libs.class.assert_error_message(count, r[:responses].length, r[:log])
          )
        end

      end
    end
  end
end
